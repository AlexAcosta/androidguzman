package com.guzman.web2android.conexiones;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.guzman.web2android.utilities.StaticResources;

public class LoginConnection {
	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String URL_LOGIN = StaticResources.url_login;
	private static String URL_PASS = StaticResources.url_pass;

	public static JSONObject Login(String usuario, String pass) {

		HttpClient httpClient = new DefaultHttpClient();

		HttpGet del = new HttpGet(IP + ":" + PUERTO + URL_LOGIN + usuario
				+ URL_PASS + pass);

		del.setHeader("content-type", "application/json");
		JSONObject respJSON = null;
		try {
			HttpResponse resp = httpClient.execute(del);
			if (resp.getEntity() != null) {
				String respStr = EntityUtils.toString(resp.getEntity());

				respJSON = new JSONObject(respStr);
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respJSON;
	}
}
