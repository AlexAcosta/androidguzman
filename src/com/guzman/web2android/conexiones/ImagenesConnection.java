package com.guzman.web2android.conexiones;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.drawable.Drawable;

import com.guzman.web2android.utilities.StaticResources;

public class ImagenesConnection {

	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String PATH = StaticResources.url_path_img;

	public static Drawable imagen(String url) {
		InputStream is = null;
		try {
			is = (InputStream) new URL(IP + ":" + PUERTO + PATH + url)
					.getContent();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Drawable.createFromStream(is, "src name");
	}

}
