package com.guzman.web2android.conexiones;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import com.guzman.web2android.utilities.StaticResources;


public class JuegosTerminadosConnection {

	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String URL_OBTENER_JUEGOS_TERMINADOS = StaticResources.url_obtener_juegos_terminados;
	private static String USUARIO = StaticResources.url_usuario;


	public static JSONArray ObtenerJuegos(Integer juego,Integer usuario) {
			
		HttpClient httpClient = new DefaultHttpClient();

		HttpGet del =new HttpGet(IP+":"+PUERTO+URL_OBTENER_JUEGOS_TERMINADOS+juego+USUARIO+usuario);

		del.setHeader("content-type", "application/json");
		JSONArray respJSON = null;
		try {
			HttpResponse resp = httpClient.execute(del);
			String respStr = EntityUtils.toString(resp.getEntity());

			respJSON = new JSONArray(respStr);
			 	
			
		}catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respJSON;		
	}	

}
