package com.guzman.web2android.ArticleFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guzman.web2android.Opciones;
import com.guzman.web2android.R;
import com.guzman.web2android.fragments.ArticleFragment;

public class OpcionesFragment extends ArticleFragment
{
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.activity_opciones, container,false);
		
		
		Opciones op = new Opciones();
		op.inicializar(getActivity(), view);
		
		return view;
	}

}
