package com.guzman.web2android.ArticleFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guzman.web2android.JuegoObjeto;
import com.guzman.web2android.R;
import com.guzman.web2android.fragments.ArticleFragment;

public class JuegoObjetoFragment extends ArticleFragment {

	private JuegoObjeto jo;
	private boolean musicaON = false;
	private boolean sonidoON = false;
	private int faseActual=0;
	
	
	@Override
	public void onStop() {
		super.onStop();
		jo.onStop();

	}
	

	@Override
	public void onResume() {
		super.onResume();
		jo.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		jo.onPause();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_juegoobjeto, container,
				false);
		jo = new JuegoObjeto();
		
		if(this.getArguments()!=null){
			faseActual=this.getArguments().getInt("faseActual");
			musicaON = this.getArguments().getBoolean("musica");
			sonidoON = this.getArguments().getBoolean("sonido");
			
		}
		jo.inicializar(getActivity(),view,faseActual,this, musicaON, sonidoON);
		return view;
	}


	
	
}
