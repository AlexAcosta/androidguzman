package com.guzman.web2android.utilities;

public class StaticResources {
	public static final String ip = "http://192.168.1.145";
    public static final String puerto = "8080";
    public static final String url_path="/ServiciosRestFULL/webresources/com.proyectoweb.entityrest.";
    public static final String url_obtener_juegos_terminados=url_path+"recuadros/idjuego=";
    public static final String url_login=url_path+"usuario/comprobar?login=";
    public static final String url_pass="&pass=";
    public static final String url_usuario = "&idusuario="; 
    public static final String url_path_img="/ProyectoWeb-war/";
}
