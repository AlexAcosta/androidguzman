package com.guzman.web2android.utilities;

import com.guzman.web2android.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.SystemClock;
import android.view.View;

public class GifView extends View
{
    Movie movie;
    
    public GifView(Context context,int id) 
    {
    	super(context);
        movie = Movie.decodeStream (
        		context.getResources().openRawResource(
                            id));
    }
        
    @Override
    protected void onDraw(Canvas canvas) 
    {   
    	if (movie != null) 
    	{
    		int x=(int) getResources().getDimension(
					R.dimen.position_gif_x);
    		int y=(int) getResources().getDimension(
					R.dimen.position_gif_y);
            Path clipPath = new Path();
            RectF rect = new RectF(x ,y, movie.width()+x, movie.height()+y);
            clipPath.addRoundRect(rect, 20.0f, 20.0f, Path.Direction.CW);
            canvas.clipPath(clipPath);
    		movie.setTime((int) SystemClock.uptimeMillis() % movie.duration());
            movie.draw(canvas,x ,y);
            invalidate();
         }
    }
}