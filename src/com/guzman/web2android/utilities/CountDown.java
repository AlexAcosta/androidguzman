package com.guzman.web2android.utilities;

import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import com.guzman.web2android.JuegoObjeto;
import com.guzman.web2android.R;

public class CountDown extends CountDownTimer {

	private long timeLeft;
	TextView textoContador;
	Context c;
	JuegoObjeto jo;
	ScaleAnimation scaleAnimation = new ScaleAnimation(1, 1.5f, 1, 1.5f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);  


	public CountDown(long millisInFuture, long countDownInterval,
			TextView tiempotxt, Context context, JuegoObjeto jo) {
		super(millisInFuture, countDownInterval);
		textoContador = tiempotxt;
		this.jo = jo;
		this.c = context;
	}

	public long getTimeLeft() {
		return timeLeft;
	}

	public void setTimeLeft(long time) {
		timeLeft = time;
	}

	@Override
	public void onFinish() {
		jo.sonido(R.raw.finopartida);
		textoContador.setText("Fin");
		jo.partidaPerdida();
	}

	@Override
	public void onTick(long millisUntilFinished) {
		textoContador.setText("" + millisUntilFinished / 1000);
		timeLeft = millisUntilFinished;
		if (timeLeft <= 4000) {
			scaleAnimation.setDuration(1000);
			textoContador.startAnimation(scaleAnimation);
			textoContador.setTextColor(Color.RED);
			jo.musica(R.raw.tiempo);
		}
			
	}
}
