package com.guzman.web2android.utilities;

import android.content.Context;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;

import com.guzman.web2android.JuegoObjeto;
import com.guzman.web2android.R;
import com.guzman.web2android.singleton.JuegoSingleton;

public class DragListener implements OnDragListener {

	Context c;
	int faseActual;
	JuegoObjeto jo;

	public DragListener(Context c, int faseActual, JuegoObjeto jo) {
		this.faseActual = faseActual;
		this.c = c;
		this.jo = jo;
	}

	@Override
	public boolean onDrag(View v, DragEvent event) {

		switch (event.getAction()) {
		case DragEvent.ACTION_DRAG_STARTED:
			break;
		case DragEvent.ACTION_DRAG_ENTERED:
			break;
		case DragEvent.ACTION_DRAG_EXITED:
			break;
		case DragEvent.ACTION_DROP:
			View view = (View) event.getLocalState();
			JuegoSingleton js = JuegoSingleton.getInstance();
			if (js.getFase().get(faseActual).getRecuadros().get(view.getId())
					.getIdImagenes().getIdImagen() == js.getFase()
					.get(faseActual).getRecuadros().get(0).getIdImagenes()
					.getIdImagen()) {
				if (faseActual == js.getFase().size() - 1) {

					jo.sonido(R.raw.aciertojuego);
				} else {

					jo.sonido(R.raw.aciertofase);
				}
				jo.partidaGanada();
			} else {

				jo.sonido(R.raw.error);
				jo.decrementointentos(v);
			}

			break;
		case DragEvent.ACTION_DRAG_ENDED:
			break;
		default:
			break;
		}
		return true;
	}
}
