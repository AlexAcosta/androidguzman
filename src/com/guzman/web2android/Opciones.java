package com.guzman.web2android;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.guzman.web2android.model.Fase;
import com.guzman.web2android.singleton.JuegoSingleton;

public class Opciones {

	
	private Spinner s1,s2,s3,s4,sFase;
	private Button btnGuardar;
	private Activity act; 
	public static int faseActual;
	
	List<Fase> listaFases;
	List<String> fasesSpinner;
	
	
	JuegoSingleton j = JuegoSingleton.getInstance();
	
	public void inicializar( final Activity act, final View v)
	{
		this.act=act;
		btnGuardar= (Button) v.findViewById(R.id.button1);
		btnGuardar.setBackgroundResource(R.drawable.custom_button);
		
		btnGuardar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				guardarOpciones();

			}
		});
		
		//ACCI�N Y PISTA
		
		s1 = (Spinner) v.findViewById(R.id.spinnerPista);
		ArrayAdapter<CharSequence> adapterS1 = ArrayAdapter.createFromResource(
				act.getApplicationContext(), R.array.array_pistas,R.layout.estilo_spinner);
		adapterS1.setDropDownViewResource(R.layout.estilo_spinner_dropdown);
		s1.setAdapter(adapterS1);
		if(j.getJuego().getPista()!=1)
			s1.setSelection(1);
			
			
		s2 = (Spinner) v.findViewById(R.id.spinnerAccion);
		ArrayAdapter<CharSequence> adapterS2 = ArrayAdapter.createFromResource(
				act.getApplicationContext(), R.array.array_accion, R.layout.estilo_spinner);
		adapterS2.setDropDownViewResource(R.layout.estilo_spinner_dropdown);
		s2.setAdapter(adapterS2);
		
		if(j.getJuego().getAccionObjeto()!=1)
			s2.setSelection(1);
		
		fasesSpinner = new ArrayList<String>();
		listaFases=j.getFase();
		
		for(Fase f:listaFases)
		{
			fasesSpinner.add(f.getNombre());
			
		}
		
		
		sFase = (Spinner) v.findViewById(R.id.spinnerFase);
		ArrayAdapter<String> adapterSFase = new ArrayAdapter<String>(
				act, R.layout.estilo_spinner, fasesSpinner);
		adapterSFase.setDropDownViewResource(R.layout.estilo_spinner_dropdown);
		sFase.setAdapter(adapterSFase);
		
		faseActual = sFase.getSelectedItemPosition();
		Fase f = listaFases.get(sFase.getSelectedItemPosition());				
		 
		s3 = (Spinner) v.findViewById(R.id.spinnerTiempo);
		ArrayAdapter<CharSequence> adapterS3 = ArrayAdapter.createFromResource(
				act.getApplicationContext(), R.array.array_tiempo, R.layout.estilo_spinner);
		adapterS3.setDropDownViewResource(R.layout.estilo_spinner_dropdown);
		s3.setAdapter(adapterS3);
		
		
		switch(f.getTiempo())
		{
			case 20: s3.setSelection(1);
					 break;
			case 30: s3.setSelection(2);
			 		 break;		 
			case 40: s3.setSelection(3);
			 		 break;
			case 50: s3.setSelection(4);
			 		 break;
			case 60: s3.setSelection(5);
			 		 break; 		 
		}
		
		s4 = (Spinner) v.findViewById(R.id.spinnerIntentos);
		ArrayAdapter<CharSequence> adapterS4 = ArrayAdapter.createFromResource(
				act.getApplicationContext(), R.array.array_intentos, R.layout.estilo_spinner);
		adapterS4.setDropDownViewResource(R.layout.estilo_spinner_dropdown);
		s4.setAdapter(adapterS4);
		 
		
		switch(f.getNintendo())
		{
			case 2: s4.setSelection(1);
					 break;
			case 3: s4.setSelection(2);
			 		 break;		 
			case 4: s4.setSelection(3);
			 		 break;
			case 5: s4.setSelection(4);
			 		 break;
			case 6: s4.setSelection(5);
			 		 break; 		 
		}
		
		
		sFase.setOnItemSelectedListener(new OnItemSelectedListener() {
			
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3){
				
				if(Opciones.faseActual!=arg2)
				{
				
					Fase f = listaFases.get(arg2);				
					 
					if(s3.getSelectedItem().toString().equals("10sg"))
						j.getFase().get(Opciones.faseActual).setTiempo(10);
					if(s3.getSelectedItem().toString().equals("20sg"))
						j.getFase().get(Opciones.faseActual).setTiempo(20);
					if(s3.getSelectedItem().toString().equals("30sg"))
						j.getFase().get(Opciones.faseActual).setTiempo(30);
					if(s3.getSelectedItem().toString().equals("40sg"))
						j.getFase().get(Opciones.faseActual).setTiempo(40);
					if(s3.getSelectedItem().toString().equals("50sg"))
						j.getFase().get(Opciones.faseActual).setTiempo(50);
					if(s3.getSelectedItem().toString().equals("60sg"))
						j.getFase().get(Opciones.faseActual).setTiempo(60);
					
					j.getFase().get(Opciones.faseActual).setNintendo(Integer.valueOf(s4.getSelectedItem().toString()));
					Opciones.faseActual = arg2;
					
					
					s3 = (Spinner) v.findViewById(R.id.spinnerTiempo);
					ArrayAdapter<CharSequence> adapterS3 = ArrayAdapter.createFromResource(
							act.getApplicationContext(), R.array.array_tiempo, R.layout.estilo_spinner);
					adapterS3.setDropDownViewResource(R.layout.estilo_spinner_dropdown);
					s3.setAdapter(adapterS3);
					
					
					switch(f.getTiempo())
					{
						case 20: s3.setSelection(1);
								 break;
						case 30: s3.setSelection(2);
						 		 break;		 
						case 40: s3.setSelection(3);
						 		 break;
						case 50: s3.setSelection(4);
						 		 break;
						case 60: s3.setSelection(5);
						 		 break; 		 
					}
					
					
					s4 = (Spinner) v.findViewById(R.id.spinnerIntentos);
					ArrayAdapter<CharSequence> adapterS4 = ArrayAdapter.createFromResource(
							act.getApplicationContext(), R.array.array_intentos, R.layout.estilo_spinner);
					adapterS4.setDropDownViewResource(R.layout.estilo_spinner_dropdown);
					s4.setAdapter(adapterS4);
					
					
					switch(f.getNintendo())
					{
						case 2: s4.setSelection(1);
								 break;
						case 3: s4.setSelection(2);
						 		 break;		 
						case 4: s4.setSelection(3);
						 		 break;
						case 5: s4.setSelection(4);
						 		 break;
						case 6: s4.setSelection(5);
						 		 break; 		 
					}
					
				
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		this.act=act;
		
		
	}
	
	public void guardarOpciones()
	{
		if(s1.getSelectedItem().toString().equals("Sacudir el m�vil"))
			j.getJuego().setPista(1);
		else
			j.getJuego().setPista(2);
		
		
		if(s2.getSelectedItem().toString().equals("Un toque"))
			j.getJuego().setAccionObjeto(1);
		else
			j.getJuego().setAccionObjeto(2);
		
		if(s3.getSelectedItem().toString().equals("10sg"))
			j.getFase().get(sFase.getSelectedItemPosition()).setTiempo(10);
		if(s3.getSelectedItem().toString().equals("20sg"))
			j.getFase().get(sFase.getSelectedItemPosition()).setTiempo(20);
		if(s3.getSelectedItem().toString().equals("30sg"))
			j.getFase().get(sFase.getSelectedItemPosition()).setTiempo(30);
		if(s3.getSelectedItem().toString().equals("40sg"))
			j.getFase().get(sFase.getSelectedItemPosition()).setTiempo(40);
		if(s3.getSelectedItem().toString().equals("50sg"))
			j.getFase().get(sFase.getSelectedItemPosition()).setTiempo(50);
		if(s3.getSelectedItem().toString().equals("60sg"))
			j.getFase().get(sFase.getSelectedItemPosition()).setTiempo(60);
		
		j.getFase().get(sFase.getSelectedItemPosition()).setNintendo(Integer.valueOf(s4.getSelectedItem().toString()));
		
		Toast.makeText(act.getApplicationContext(), "Guardado", Toast.LENGTH_SHORT).show();
		
	}
	
}
