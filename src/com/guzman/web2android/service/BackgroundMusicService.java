package com.guzman.web2android.service;


import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Binder;


public class BackgroundMusicService extends MusicService {
	
	public BackgroundMusicService(){
	}	
	
	public class ServiceBinder extends Binder {
		public BackgroundMusicService getService() {
			return BackgroundMusicService.this;
		}
	}


	@Override
	public void onCreate() {
		super.onCreate();		
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)

	{
		idmusica = (Integer) intent.getExtras().get("cancion");
		
		mPlayer = MediaPlayer.create(this, idmusica);
		mPlayer.setOnErrorListener(this);

		if (mPlayer != null) {
				mPlayer.setLooping(true);
			mPlayer.setVolume(100, 100);
		}

		mPlayer.setOnErrorListener(new OnErrorListener() {

			public boolean onError(MediaPlayer mp, int what, int extra) {

				onError(mPlayer, what, extra);
				return true;
			}
		});	
		mPlayer.start();
		return START_STICKY;

	}
}
