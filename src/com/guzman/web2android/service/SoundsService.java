package com.guzman.web2android.service;


import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Binder;


public class SoundsService extends MusicService {
	
	public SoundsService(){
	}	
	
	public class ServiceBinder extends Binder {
		public SoundsService getService() {
			return SoundsService.this;
		}
	}


	@Override
	public void onCreate() {
		super.onCreate();		
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)

	{
		idmusica = (Integer) intent.getExtras().get("cancion");
		
		mPlayer = MediaPlayer.create(this, idmusica);
		mPlayer.setOnErrorListener(this);

		if (mPlayer != null) {
				mPlayer.setLooping(false);
			mPlayer.setVolume(100, 100);
		}

		mPlayer.setOnErrorListener(new OnErrorListener() {

			public boolean onError(MediaPlayer mp, int what, int extra) {

				onError(mPlayer, what, extra);
				return true;
			}
		});	
		mPlayer.start();
		return START_STICKY;

	}
}
