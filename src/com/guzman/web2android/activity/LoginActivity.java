package com.guzman.web2android.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.guzman.web2android.R;
import com.guzman.web2android.asynctaks.LoginAsyncTask;

public class LoginActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		Button btnlogin = (Button) findViewById(R.id.btnLogin);
		Button btnExit = (Button) findViewById(R.id.btnExit);
		btnlogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String user = ((EditText) findViewById(R.id.txtUsername))
						.getText().toString();
				String password = ((EditText) findViewById(R.id.txtPassword))
						.getText().toString();
				LoginAsyncTask tarea = new LoginAsyncTask(LoginActivity.this);
				tarea.execute(user, password);

			}
		});

		btnExit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();

			}
		});

	}

}
