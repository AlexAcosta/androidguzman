package com.guzman.web2android.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

import com.guzman.web2android.JuegoObjeto;
import com.guzman.web2android.R;

public class JuegoObjetoActivity extends Activity {

	private JuegoObjeto jo;
	private int faseActual = 0;
	private boolean musicaON = false;
	private boolean sonidoON = false;
	private boolean siguientefase=false;

	
	@Override
	protected void onStop() {
		super.onStop();
		jo.onStop();

	}
	@Override
	public void onConfigurationChanged(Configuration newConfig){
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		jo.atras();
	}

	@Override
	protected void onResume() {
		super.onResume();
		jo.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		jo.onPause();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_juegoobjeto);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		ActionBar a = getActionBar();
		a.hide();
		if (getIntent().getExtras() != null) {
			faseActual = getIntent().getExtras().getInt("faseActual");
			musicaON = getIntent().getExtras().getBoolean("musica");
			sonidoON = getIntent().getExtras().getBoolean("sonido");
		}
		View v = this.findViewById(android.R.id.content).getRootView();
		jo = new JuegoObjeto();
		jo.inicializar(this, v, faseActual, null, musicaON, sonidoON);

	}

	
}
