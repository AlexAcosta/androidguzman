package com.guzman.web2android.singleton;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.guzman.web2android.model.Fase;
import com.guzman.web2android.model.Imagenes;
import com.guzman.web2android.model.Juego;
import com.guzman.web2android.model.Recuadros;
import com.guzman.web2android.model.Usuario;

public class JuegoSingleton {

	private static JuegoSingleton INSTANCE = null;

	private List<Fase> fase;
	private Juego juego;
	private Usuario usuario;
	int faseAcutal;

	private JuegoSingleton() {
		juego = new Juego();
		fase = new ArrayList<Fase>();
		usuario = new Usuario();
	}

	private synchronized static void createInstance() {
		if (INSTANCE == null) {
			INSTANCE = new JuegoSingleton();
		}
	}

	public static JuegoSingleton getInstance() {
		createInstance();
		return INSTANCE;
	}

	public List<Fase> getFase() {
		return fase;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setFase(List<Fase> fase) {
		this.fase = fase;
	}

	public Juego getJuego() {
		return juego;
	}

	public void setJuego(Juego juego) {
		this.juego = juego;
	}

	public void initUsu(JSONObject datos) {
		try {
			this.usuario.setApellido(datos.getString("apellido"));
			this.usuario.setNombre(datos.getString("nombre"));
			this.usuario.setLogin(datos.getString("login"));
			this.usuario.setIdUsuario(datos
					.getInt("idusuario"));
			this.usuario.setCorreo(datos.getString("correo"));
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public void init(JSONArray datos) {
		if (datos.length() > 0) {
			JSONObject juegoJSON;
			JSONObject usuarioJSON;
			JSONObject faseJSON;
			JSONObject fondoJSON;
			JSONObject imagenJSON;
			try {
				juegoJSON = datos.getJSONObject(0).getJSONObject("idfase")
						.getJSONObject("idjuego");
				this.juego.setIdJuego(juegoJSON.getInt("idjuego"));
				this.juego.setPista(Integer.parseInt(juegoJSON
						.getString("pista")));
				this.juego.setAccionObjeto(Integer.parseInt(juegoJSON
						.getString("accionobjeto")));
				usuarioJSON = datos.getJSONObject(0).getJSONObject("idfase")
						.getJSONObject("idusuario");
				this.juego.setIdUsuario(usuarioJSON.getInt("idusuario"));
				for (int i = 0; i < datos.length(); i++) {
					Recuadros recuadro = new Recuadros();
					recuadro.setIdRecuadro(datos.getJSONObject(i).getInt(
							"idrecuadro"));
					recuadro.setOrden(datos.getJSONObject(i).getInt("orden"));
					recuadro.setIdFase(datos.getJSONObject(i)
							.getJSONObject("idfase").getInt("idfase"));
					imagenJSON = datos.getJSONObject(i).getJSONObject(
							"idimagenes");
					Imagenes imagen = new Imagenes();
					imagen.setDescripcion(imagenJSON.getString("descripcion"));
					imagen.setDireccion(imagenJSON.getString("direccion"));
					imagen.setIdImagen(imagenJSON.getInt("idimagen"));
					imagen.setIdTipo(imagenJSON.getJSONObject("idtipo").getInt(
							"idtipo"));
					recuadro.setIdImagenes(imagen);
					Fase fa = new Fase();
					int aux = 0;
					while (aux < this.fase.size()
							&& fase.get(aux).getIdFase() != recuadro
									.getIdFase()) {
						aux++;
					}
					if (aux < this.fase.size()) {
						this.fase.get(aux).getRecuadros().add(recuadro);
					} else {
						faseJSON = datos.getJSONObject(i).getJSONObject(
								"idfase");
						fa.setIdFase(faseJSON.getInt("idfase"));
						if (faseJSON.has("idfondo")) {
							fondoJSON = faseJSON.getJSONObject("idfondo");
							Imagenes fondo = new Imagenes();
							fondo.setDescripcion(fondoJSON
									.getString("descripcion"));
							fondo.setDireccion(fondoJSON.getString("direccion"));
							fondo.setIdImagen(fondoJSON.getInt("idimagen"));
							fondo.setIdTipo(fondoJSON.getJSONObject("idtipo")
									.getInt("idtipo"));
							fa.setIdFondo(fondo);
						}
						fa.setIdJuego(juego.getIdJuego());
						fa.setNintendo(faseJSON.getInt("nintento"));
						fa.setNobjeto(faseJSON.getInt("nobjeto"));
						fa.setNombre(faseJSON.getString("nombre"));
						fa.setTiempo(faseJSON.getInt("tiempo"));
						if (fa.getRecuadros() == null) {
							List<Recuadros> lr = new ArrayList<Recuadros>();
							fa.setRecuadros(lr);
						}
						fa.getRecuadros().add(recuadro);
						this.fase.add(fa);
					}
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

}
