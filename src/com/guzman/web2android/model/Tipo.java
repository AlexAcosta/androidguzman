package com.guzman.web2android.model;

public class Tipo {
	private int idTipo;
	private String Nombre;
	
	public Tipo(int idTipo, String Nombre){
		this.idTipo=idTipo;
		this.Nombre=Nombre;
		
	}
	
	public int getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
	

}
