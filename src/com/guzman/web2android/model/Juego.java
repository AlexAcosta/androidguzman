package com.guzman.web2android.model;

public class Juego {
	
	private int idJuego;
	private int idNombre;
	private int pista;
	private int accionObjeto;
	private int terminado;
	private int idUsuario;
	private String direccionImagen;
	
	public Juego(){
		
	}
	
	public Juego(int idJuego, int idNombre, int pista, int accionObjeto, int terminado, int idUsuario, String direccionImagen){
		this.idJuego=idJuego;
		this.idNombre=idNombre;
		this.pista=pista;
		this.accionObjeto=accionObjeto;
		this.terminado=terminado;
		this.idUsuario=idUsuario;
		this.direccionImagen=direccionImagen;
		
	}
	
	public int getIdJuego() {
		return idJuego;
	}
	public void setIdJuego(int idJuego) {
		this.idJuego = idJuego;
	}
	public int getIdNombre() {
		return idNombre;
	}
	public void setIdNombre(int idNombre) {
		this.idNombre = idNombre;
	}
	public int getPista() {
		return pista;
	}
	public void setPista(int pista) {
		this.pista = pista;
	}
	public int getAccionObjeto() {
		return accionObjeto;
	}
	public void setAccionObjeto(int accionObjeto) {
		this.accionObjeto = accionObjeto;
	}
	public int getTerminado() {
		return terminado;
	}
	public void setTerminado(int terminado) {
		this.terminado = terminado;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getDireccionImagen() {
		return direccionImagen;
	}
	public void setDireccionImagen(String direccionImagen) {
		this.direccionImagen = direccionImagen;
	}
	
	

}
