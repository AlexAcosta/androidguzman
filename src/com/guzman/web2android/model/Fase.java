package com.guzman.web2android.model;

import java.util.List;

public class Fase {
	private int idFase;
	private String nombre;
	private int idJuego;
	private int tiempo;
	private int nintendo;
	private int nobjeto;
	private Imagenes idFondo;
	private int idTipoCarta;
	private int idUsuario;
	private List<Recuadros> recuadros;

	
	public Fase(){
	}
	
	public Fase (int idFase, String nombre, int idJuego, int tiempo, int nintiendo, int nobjeto,Imagenes idFondo, int idTipoCarta, int idUsuario, List<Recuadros> recuadros){
		this.idFase=idFase;
		this.nombre= nombre;
		this.idJuego=idJuego;
		this.tiempo=tiempo;
		this.nintendo=nintiendo;
		this.nobjeto=nobjeto;
		this.idFondo=idFondo;
		this.idTipoCarta=idTipoCarta;
		this.idUsuario=idUsuario;
		this.recuadros=recuadros;
	}
	
	public int getIdFase() {
		return idFase;
	}
	public void setIdFase(int idFase) {
		this.idFase = idFase;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getIdJuego() {
		return idJuego;
	}
	public void setIdJuego(int idJuego) {
		this.idJuego = idJuego;
	}
	public int getTiempo() {
		return tiempo;
	}
	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}
	public int getNintendo() {
		return nintendo;
	}
	public void setNintendo(int nintendo) {
		this.nintendo = nintendo;
	}
	public int getNobjeto() {
		return nobjeto;
	}
	public void setNobjeto(int nobjeto) {
		this.nobjeto = nobjeto;
	}
	public Imagenes getIdFondo() {
		return idFondo;
	}
	public void setIdFondo(Imagenes idFondo) {
		this.idFondo = idFondo;
	}
	public int getIdTipoCarta() {
		return idTipoCarta;
	}
	public void setIdTipoCarta(int idTipoCarta) {
		this.idTipoCarta = idTipoCarta;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public List<Recuadros> getRecuadros() {
		return recuadros;
	}

	public void setRecuadros(List<Recuadros> recuadros) {
		this.recuadros = recuadros;
	}
	
	
	

}
