package com.guzman.web2android.model;

public class Usuario {
	private int idUsuario;
	private String login;
	private String pass;
	private String nombre;
	private String apellido;
	private String correo;
	
	public Usuario(int idUsuario, String login, String pass, String nombre, String apellido, String correo){
		this.idUsuario=idUsuario;
		this.login=login;
		this.pass=pass;
		this.nombre=nombre;
		this.apellido=apellido;
		this.correo=correo;
		
	}
	
	public Usuario() {
		
	}

	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	

}
