package com.guzman.web2android.model;

public class NombreJuego {
	private int idNombreJuego;
	private String nombre;
	
	public NombreJuego(int idNombreJuego, String nombre){
		this.idNombreJuego=idNombreJuego;
		this.nombre=nombre;
	}
	
	public int getIdNombreJuego() {
		return idNombreJuego;
	}
	public void setIdNombreJuego(int idNombreJuego) {
		this.idNombreJuego = idNombreJuego;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
