package com.guzman.web2android.model;

public class Imagenes {
	private int idImagen;
	private int idUsuario;
	private int idTipo;
	private String direccion;
	private String descripcion;
	
	public Imagenes(){
	}
	
	public Imagenes(int idImagen, int idUsuario, int idTipo, String direccion, String descripcion){
		this.idImagen=idImagen;
		this.idUsuario=idUsuario;
		this.idTipo=idTipo;
		this.direccion=direccion;
		this.descripcion=descripcion;
		
		
	}
	
	public int getIdImagen() {
		return idImagen;
	}
	public void setIdImagen(int idImagen) {
		this.idImagen = idImagen;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
