package com.guzman.web2android.model;

public class Recuadros {
	private int idRecuadro;
	private Imagenes idImagenes;
	private int orden;
	private int idFase;
	
	public Recuadros(){
	}
	
	public Recuadros(int idRecuadro,Imagenes idImagenes,int orden, int idFase){
		this.idRecuadro=idRecuadro;
		this.idImagenes=idImagenes;
		this.orden=orden;
		this.idFase=idFase;
		
	}
	
	public int getIdRecuadro() {
		return idRecuadro;
	}
	public void setIdRecuadro(int idRecuadro) {
		this.idRecuadro = idRecuadro;
	}
	public Imagenes getIdImagenes() {
		return idImagenes;
	}
	public void setIdImagenes(Imagenes idImagenes) {
		this.idImagenes = idImagenes;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getIdFase() {
		return idFase;
	}
	public void setIdFase(int idFase) {
		this.idFase = idFase;
	}
	
	
}
