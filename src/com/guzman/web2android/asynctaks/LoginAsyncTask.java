package com.guzman.web2android.asynctaks;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.guzman.web2android.conexiones.LoginConnection;
import com.guzman.web2android.fragments.MainActivity;
import com.guzman.web2android.singleton.JuegoSingleton;

public class LoginAsyncTask extends AsyncTask<String, Void, JSONObject> {

	ProgressDialog dialog;
	Context c;

	public LoginAsyncTask(Context c) {
		this.c = c;
		dialog = new ProgressDialog(c);
	}

	protected void onPreExecute() {

		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Logueando...");
		dialog.setIndeterminate(true);
		dialog.show();

	}

	@Override
	protected JSONObject doInBackground(String... params) {
		return LoginConnection.Login(params[0], params[1]);

	}

	protected void onPostExecute(JSONObject result) {

		if (result != null) {
			JuegoSingleton js = JuegoSingleton.getInstance();
			js.initUsu(result);

			JuegosTerminadosAsyncTask tarea = new JuegosTerminadosAsyncTask(c);

			tarea.execute(1, js.getUsuario().getIdUsuario());
			Intent intent = new Intent(c, MainActivity.class);

			c.startActivity(intent);

		} else {
			Toast.makeText(c, "Usuario o contraseņa invalidos",
					Toast.LENGTH_LONG).show();

		}
		if (this.dialog.isShowing()) {
			this.dialog.dismiss();
		}

	}

}