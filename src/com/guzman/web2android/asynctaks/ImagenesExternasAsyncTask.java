package com.guzman.web2android.asynctaks;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import com.guzman.web2android.conexiones.ImagenesConnection;

public class ImagenesExternasAsyncTask extends
		AsyncTask<String, Void, Drawable> {

	@Override
	protected Drawable doInBackground(String... arg0) {
		return ImagenesConnection.imagen(arg0[0]);

	}

}
