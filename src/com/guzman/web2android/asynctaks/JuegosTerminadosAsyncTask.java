package com.guzman.web2android.asynctaks;

import org.json.JSONArray;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.guzman.web2android.conexiones.JuegosTerminadosConnection;
import com.guzman.web2android.fragments.ArticleFragment;
import com.guzman.web2android.singleton.JuegoSingleton;

public class JuegosTerminadosAsyncTask extends
		AsyncTask<Integer, Integer, JSONArray> {

	ProgressDialog dialog;
	Context c;
	ArticleFragment fragment;

	public JuegosTerminadosAsyncTask(Context c) {
		this.c = c;
		dialog = new ProgressDialog(c);
	}

	@Override
	protected JSONArray doInBackground(Integer... arg) {
		return JuegosTerminadosConnection.ObtenerJuegos(arg[0], arg[1]);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Cargando entradas...");
		dialog.setIndeterminate(true);
		dialog.show();
	}

	protected void onPostExecute(JSONArray result) {
		super.onPostExecute(result);

		JuegoSingleton j = JuegoSingleton.getInstance();
		j.init(result);

		if (this.dialog.isShowing()) {
			this.dialog.dismiss();
		}
	}
}
