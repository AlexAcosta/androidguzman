package com.guzman.web2android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guzman.web2android.R;
import com.guzman.web2android.ArticleFragment.JuegoObjetoFragment;
import com.guzman.web2android.ArticleFragment.OpcionesFragment;
import com.guzman.web2android.asynctaks.JuegosTerminadosAsyncTask;
import com.guzman.web2android.singleton.JuegoSingleton;

public class ArticleFragment extends Fragment {
	final static String ARG_POSITION = "position";
	int mCurrentPosition = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// If activity recreated (such as from screen rotate), restore
		// the previous article selection set by onSaveInstanceState().
		// This is primarily necessary when in the two-pane layout.
		if (savedInstanceState != null) {
			mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
		}

		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.article_view, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();

		// During startup, check if there are arguments passed to the fragment.
		// onStart is a good place to do this because the layout has already
		// been
		// applied to the fragment at this point so we can safely call the
		// method
		// below that sets the article text.
		Bundle args = getArguments();
		if (args != null) {
			// Set article based on argument passed in
			updateArticleView(args.getInt(ARG_POSITION));
		} else if (mCurrentPosition != -1) {
			// Set article based on saved instance state defined during
			// onCreateView
			updateArticleView(mCurrentPosition);
		}
	}

	public void updateArticleView(int position) {
		// TextView article = (TextView)
		// getActivity().findViewById(R.id.article);
		// article.setText(Ipsum.Articles[position]);
		// mCurrentPosition = position;

		// Aqui debemos cargar los diferentes Activities
		// ArticleFragment fr;

		switch (position) {
		case 0:
			// fr = new GameActivity();
			// android.support.v4.app.FragmentManager fm1 =
			// getFragmentManager();
			// android.support.v4.app.FragmentTransaction ft1 =
			// fm1.beginTransaction();
			// ft1.replace(R.id.article_fragment, fr)
			// .addToBackStack(null)
			// .commit();

			ArticleFragment fr;
			fr = new JuegoObjetoFragment();
			this.getFragmentManager().beginTransaction()
					.replace(R.id.article_fragment, fr).addToBackStack(null)
					.commit();

			break;
		 case 1:
		// fr = new fragmet1();
		// android.support.v4.app.FragmentManager fm2 = getFragmentManager();
		// android.support.v4.app.FragmentTransaction ft2 =
		// fm2.beginTransaction();
		// ft2.replace(R.id.article_fragment, fr)
		// .addToBackStack(null)
		// .commit();
		
			ArticleFragment fr1;
			fr1 = new OpcionesFragment();
			this.getFragmentManager().beginTransaction()
					.replace(R.id.article_fragment, fr1).addToBackStack(null)
					.commit();

			
		break;
		 case 2:
			 Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			 break;
		// case 2:
		// fr = new ArticleFragment();
		// android.support.v4.app.FragmentManager fm3 = getFragmentManager();
		// android.support.v4.app.FragmentTransaction ft3 =
		// fm3.beginTransaction();
		// ft3.replace(R.id.article_fragment, fr)
		// .addToBackStack(null)
		// .commit();
		// break;
		default:
			break;
		}
		//
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// Save the current article selection in case we need to recreate the
		// fragment
		outState.putInt(ARG_POSITION, mCurrentPosition);
	}
}
