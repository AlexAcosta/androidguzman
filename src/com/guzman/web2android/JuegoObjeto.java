package com.guzman.web2android;

import java.util.Date;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.guzman.web2android.ArticleFragment.JuegoObjetoFragment;
import com.guzman.web2android.activity.JuegoObjetoActivity;
import com.guzman.web2android.asynctaks.ImagenesExternasAsyncTask;
import com.guzman.web2android.fragments.ArticleFragment;
import com.guzman.web2android.fragments.MainActivity;
import com.guzman.web2android.service.BackgroundMusicService;
import com.guzman.web2android.service.SoundsService;
import com.guzman.web2android.singleton.JuegoSingleton;
import com.guzman.web2android.utilities.CountDown;
import com.guzman.web2android.utilities.DragListener;
import com.guzman.web2android.utilities.DropListener;
import com.guzman.web2android.utilities.GifView;
import com.guzman.web2android.utilities.ShakeEventListener;

public class JuegoObjeto {

	public Activity act;

	private JuegoSingleton js = JuegoSingleton.getInstance();
	private int faseActual = 0;
	private Boolean pistaUsada = false;
	private Intent music;
	private Vector<Integer> vsolucion;
	private Boolean musicaON;
	private Boolean SonidoON;
	private SensorManager mSensorManager;
	private int partidaGanada;
	private CountDown contador;
	private ShakeEventListener mSensorListener;
	private boolean mIsBound = false;
	private BackgroundMusicService mServ;
	private JuegoObjetoFragment jof;
	private boolean siguientefase;

	public ServiceConnection Scon;

	public void doBindService() {
		act.bindService(new Intent(act, BackgroundMusicService.class), Scon,
				Context.BIND_AUTO_CREATE);
		mIsBound = true;
	}

	public void doUnbindService() {
		if (mIsBound) {
			act.unbindService(Scon);
			mIsBound = false;
		}
	}

	public void inicializar(Activity act, View v, int faseActual,
			JuegoObjetoFragment jof, boolean musica, boolean sonido) {
		this.act = act;
		this.faseActual = faseActual;
		this.jof = jof;
		this.musicaON = musica;
		this.SonidoON = sonido;
		this.siguientefase = false;
		estadosbotones(v);

		ImagenesExternasAsyncTask imgext = new ImagenesExternasAsyncTask();
		try {
			v.setBackground(imgext.execute(
					js.getFase().get(faseActual).getIdFondo().getDireccion())
					.get());

		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Scon = new ServiceConnection() {

			public void onServiceConnected(ComponentName name, IBinder binder) {
				mServ = ((BackgroundMusicService.ServiceBinder) binder)
						.getService();
			}

			public void onServiceDisconnected(ComponentName name) {
				mServ = null;
			}
		};

		musica(R.raw.backgroundsound);
		Button btnmusica = (Button) v.findViewById(R.id.botonMusica);
		btnmusica.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				playPauseMusica(v);

			}
		});

		Button btnSonido = (Button) v.findViewById(R.id.botonSonido);
		btnSonido.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				playPauseSonido(v);

			}
		});

		vsolucion = new Vector<Integer>();

		TextView intentos = (TextView) v.findViewById(R.id.textintentos);
		intentos.setText(String.valueOf(js.getFase().get(faseActual)
				.getNintendo()));
		intentos.setContentDescription("numero de intentos"
				+ intentos.getText());
		TextView nombrefase = (TextView) v.findViewById(R.id.nombrefase);
		TextView pista = (TextView) v.findViewById(R.id.pista);

		nombrefase.setText(js.getFase().get(faseActual).getNombre());

		if (js.getJuego().getPista() == 1) {
			pista.setText("Agitar para obtener pista");
			pista.setTextColor(Color.WHITE);
			pista.setPadding(80, 0, 0, 0);
			mSensorManager = (SensorManager) act
					.getSystemService(Context.SENSOR_SERVICE);
			mSensorListener = new ShakeEventListener();

			mSensorListener
					.setOnShakeListener(new ShakeEventListener.OnShakeListener() {

						public void onShake() {

							QuitarFotos();
						}
					});
		} else {
			LinearLayout ly = (LinearLayout) v
					.findViewById(R.id.barrasuperiorButton);
			Button btnPista = new Button(act);
			btnPista.setText("Pista");
			btnPista.setBackgroundResource(R.drawable.custom_button);
			btnPista.setLayoutParams(new LayoutParams(
					LayoutParams.WRAP_CONTENT,
					android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT));
			btnPista.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					QuitarFotos();

				}
			});
			ly.addView(btnPista);
		}

		ViewGroup fijo = (ViewGroup) v.findViewById(R.id.fijo);
		fijo.setOnDragListener(new DragListener(act.getBaseContext(),
				faseActual, this));
		pintarRecuadros(fijo, 0, 0);

		if (js.getFase().get(faseActual).getNobjeto() <= 6) {
			ViewGroup fila1 = (ViewGroup) v.findViewById(R.id.fila1);
			pintarRecuadros(fila1, js.getFase().get(faseActual).getNobjeto(), 1);
		} else {
			ViewGroup fila1 = (ViewGroup) v.findViewById(R.id.fila1);
			pintarRecuadros(fila1, js.getFase().get(faseActual).getNobjeto(), 1);
			ViewGroup fila2 = (ViewGroup) v.findViewById(R.id.fila2);
			pintarRecuadros(fila2,
					js.getFase().get(faseActual).getNobjeto() - 5, 2);
		}

		TextView tiempo = (TextView) v.findViewById(R.id.texttiempo);
		tiempo.setText(String.valueOf(js.getFase().get(faseActual).getTiempo()));
		contador = new CountDown(
				Long.parseLong(tiempo.getText().toString()) * 1000, 1000,
				tiempo, act.getBaseContext(), this);
		contador.start();

	}

	private void pintarRecuadros(ViewGroup view, int img, int opcion) {

		int maxfila;
		if (opcion == 1) {
			img = 1;
			if (js.getFase().get(faseActual).getNobjeto() > 6)
				maxfila = 6;
			else
				maxfila = js.getFase().get(faseActual).getNobjeto();
		} else
			maxfila = js.getFase().get(faseActual).getNobjeto();
		for (int i = img; i <= maxfila; i++) {
			LayoutInflater inflaterFijo = LayoutInflater.from(act);
			int id = R.layout.layout_dynamic;

			RelativeLayout relativeLayout = (RelativeLayout) inflaterFijo
					.inflate(id, null, false);

			ImageView imgView = (ImageView) relativeLayout
					.findViewById(R.id.imageView);
			LinearLayout imgViewLayout = (LinearLayout) relativeLayout
					.findViewById(R.id.imageViewLayout);

			if (js.getFase().get(faseActual).getRecuadros().get(i)
					.getIdImagenes().getIdImagen() == js.getFase()
					.get(faseActual).getRecuadros().get(0).getIdImagenes()
					.getIdImagen()) {
				vsolucion.add(i);
			}

			ImagenesExternasAsyncTask imgext = new ImagenesExternasAsyncTask();
			try {
				imgView.setImageDrawable((imgext.execute(js.getFase()
						.get(faseActual).getRecuadros().get(i).getIdImagenes()
						.getDireccion()).get()));
				imgView.setContentDescription(js.getFase().get(faseActual)
						.getRecuadros().get(i).getIdImagenes().getDescripcion());
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ExecutionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			imgView.setId(i);
			imgViewLayout.setId(i);

			if (opcion != 0) {
				if (js.getJuego().getAccionObjeto() == 1) {
					imgView.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if (js.getFase().get(faseActual).getRecuadros()
									.get(v.getId()).getIdImagenes()
									.getIdImagen() == js.getFase()
									.get(faseActual).getRecuadros().get(0)
									.getIdImagenes().getIdImagen()) {
								if (faseActual == js.getFase().size() - 1) {
									sonido(R.raw.aciertojuego);
								} else {
									sonido(R.raw.aciertofase);
								}
								partidaGanada();
							} else {

								sonido(R.raw.error);
								decrementointentos(v);
							}
						}
					});
				} else {
					imgViewLayout.setOnTouchListener(new DropListener());
				}
			}

			LinearLayout.LayoutParams paramsfijomargin = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

			if (opcion == 0)
				paramsfijomargin.setMargins(0, (int) act.getResources()
						.getDimension(R.dimen.margen_superior_recuadro_fijo),
						0, 0);
			else if (opcion == 1)
				paramsfijomargin.setMargins(
						(int) act.getResources().getDimension(
								R.dimen.margen_izq_recuadro_1fila),
						(int) act.getResources().getDimension(
								R.dimen.margen_superior_recuadro_1fila), 0, 0);
			else
				paramsfijomargin.setMargins(
						(int) act.getResources().getDimension(
								R.dimen.margen_izq_recuadro_2fila),
						(int) act.getResources().getDimension(
								R.dimen.margen_superior_recuadro_2fila), 0, 0);
			view.addView(relativeLayout, paramsfijomargin);

			if (opcion == 0)
				break;
		}
	}

	public void decrementointentos(View v) {

		Animation shake = AnimationUtils.loadAnimation(act, R.anim.shake);
		v.findViewById(v.getId()).startAnimation(shake);

		TextView txtintentos = (TextView) act.findViewById(R.id.textintentos);
		int numintentos = Integer.parseInt(txtintentos.getText().toString());
		numintentos--;
		if (numintentos == 0) {
			sonido(R.raw.finopartida);
			partidaPerdida();
		} else {
			txtintentos.setText(String.valueOf(numintentos));
		}
	}

	public void partidaPerdida() {
		pararJuego();
		mostrarDialogo(R.drawable.partidaperdida, "Comenzar de nuevo");
		partidaGanada = 1;
	}

	public void partidaGanada() {
		String dialogo;
		pararJuego();
		if (faseActual == js.getFase().size() - 1) {
			dialogo = "Comenzar de nuevo";
			partidaGanada = 3;
		} else {
			dialogo = "Siguiente Fase";
			partidaGanada = 2;
		}
		mostrarDialogo(R.drawable.congratulations, dialogo);
	}

	private void pararJuego() {
		act.stopService(music);
		contador.cancel();
	}

	private void mostrarDialogo(int id, String dialogo) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				new ContextThemeWrapper(act, R.style.AlertDialogCustom));
		builder.setCancelable(false);
		GifView g = new GifView(act, id);
		g.setLayoutParams(new LayoutParams(10, 10));

		builder.setView(g);
		builder.setPositiveButton(dialogo,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						pasarSiguienteFase();
					}
				});

		AlertDialog alert = builder.create();
		alert.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
		alert.show();
		((Button) alert.findViewById(android.R.id.button1))
				.setBackgroundResource(R.drawable.custom_button);
		((Button) alert.findViewById(android.R.id.button1))
				.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		((Button) alert.findViewById(android.R.id.button1))
				.setTextSize((int) act.getResources().getDimension(
						R.dimen.letraalerta));

		WindowManager.LayoutParams l = new WindowManager.LayoutParams();
		l.copyFrom(alert.getWindow().getAttributes());
		l.width = (int) act.getResources().getDimension(R.dimen.width_alert);
		l.height = (int) act.getResources().getDimension(R.dimen.height_alert);
		alert.getWindow().setAttributes(l);
	}

	private void QuitarFotos() {
		if (!pistaUsada) {
			sonido(R.raw.pistaboton);
			int numaquitar = (int) (js.getFase().get(faseActual).getNobjeto() * 0.3);
			int numobjetos = js.getFase().get(faseActual).getNobjeto();

			Vector<Integer> v = new Vector<Integer>();

			Random random = new Random();
			random.setSeed(new Date().getTime());
			while (v.size() < numaquitar) {
				int temp = random.nextInt(numobjetos);
				if (!v.contains(temp) && temp != 0 && temp < 13
						&& !vsolucion.contains(temp))
					v.addElement(temp);
			}

			for (int i = 0; i < v.size(); i++) {
				final LinearLayout imgViewLayout = (LinearLayout) act
						.findViewById(v.get(i));

				int fadeInDuration = 1500;

				Animation fadeIn = new AlphaAnimation(1, 0);
				fadeIn.setInterpolator(new DecelerateInterpolator());
				fadeIn.setDuration(fadeInDuration);

				AnimationSet animation = new AnimationSet(false);
				animation.addAnimation(fadeIn);

				animation.setRepeatCount(1);

				animation.setAnimationListener(new AnimationListener() {
					public void onAnimationEnd(Animation animation) {
						imgViewLayout.setVisibility(View.GONE);
					}

					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub
					}

					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
					}
				});

				imgViewLayout.startAnimation(animation);

			}
			pistaUsada = true;
		}

	}

	private void playPauseSonido(View v) {

		if (SonidoON) {
			SonidoON = false;
			v.setBackgroundResource(R.drawable.sonido);
			v.setContentDescription(act
					.getString(R.string.DescriptionSonidoPlay));
		} else {
			SonidoON = true;
			v.setBackgroundResource(R.drawable.sinsonido);
			v.setContentDescription(act
					.getString(R.string.DescriptionSonidoPause));
		}
	}

	private void playPauseMusica(View v) {
		if (musicaON) {
			act.startService(music);
			musicaON = false;
			v.setBackgroundResource(R.drawable.play);
			v.setContentDescription(act
					.getString(R.string.DescriptionMusicaPlay));
		} else {
			act.stopService(music);
			musicaON = true;
			v.setBackgroundResource(R.drawable.pause);
			v.setContentDescription(act
					.getString(R.string.DescriptionMusicaPlay));
		}
	}

	public void sonido(int id) {
		if (!SonidoON) {
			Intent sonido = new Intent();
			sonido.putExtra("cancion", id);
			sonido.setClass(act, SoundsService.class);

			act.stopService(sonido);
			act.startService(sonido);
		}
	}

	public void musica(int id) {

		music = new Intent();
		music.putExtra("cancion", id);
		music.setClass(act, BackgroundMusicService.class);
		if (!musicaON) {
			act.stopService(music);
			act.startService(music);
		}
	}

	private void pasarSiguienteFase() {

		if (act.getString(R.string.screen_type).equals("phone")) {
			Intent i;
			switch (partidaGanada) {
			case 1:
				i = new Intent(act, JuegoObjetoActivity.class);
				i.putExtra("faseActual", 0);
				i.putExtra("musica", musicaON);
				i.putExtra("sonido", SonidoON);
				siguientefase = true;
				act.startActivity(i);
				act.finish();
				break;
			case 2:
				i = new Intent(act, JuegoObjetoActivity.class);
				i.putExtra("faseActual", faseActual + 1);
				i.putExtra("musica", musicaON);
				i.putExtra("sonido", SonidoON);
				siguientefase = true;
				act.startActivity(i);
				act.finish();
				break;

			case 3:
				i = new Intent(act, MainActivity.class);
				siguientefase = false;
				act.startActivity(i);
				act.finish();
				break;
			}
		} else {
			ArticleFragment fr;
			Bundle b;
			switch (partidaGanada) {
			case 1:
				fr = new JuegoObjetoFragment();
				b = new Bundle();
				b.putInt("faseActual", 0);
				b.putBoolean("musica", musicaON);
				b.putBoolean("sonido", SonidoON);
				siguientefase = true;
				b.putInt("position", -1);
				fr.setArguments(b);
				jof.getFragmentManager().beginTransaction()
						.replace(R.id.article_fragment, fr)
						.addToBackStack(null).commit();

				break;
			case 2:
				fr = new JuegoObjetoFragment();
				b = new Bundle();
				b.putInt("faseActual", faseActual + 1);
				b.putBoolean("musica", musicaON);
				b.putBoolean("sonido", SonidoON);
				siguientefase = true;
				b.putInt("position", -1);
				fr.setArguments(b);
				jof.getFragmentManager().beginTransaction()
						.replace(R.id.article_fragment, fr)
						.addToBackStack(null).commit();
				break;
			case 3:
				Intent i;
				i = new Intent(act, MainActivity.class);
				siguientefase = false;
				act.startActivity(i);
				act.finish();
				break;
			
			}
		}

	}

	private void estadosbotones(View v) {
		View buttonsonido = (View) v.findViewById(R.id.botonSonido);
		if (SonidoON) {

			buttonsonido.setBackgroundResource(R.drawable.sinsonido);
			buttonsonido.setContentDescription(act
					.getString(R.string.DescriptionSonidoPause));
		} else {

			buttonsonido.setBackgroundResource(R.drawable.sonido);
			buttonsonido.setContentDescription(act
					.getString(R.string.DescriptionSonidoPlay));

		}
		View buttonmusica = (View) v.findViewById(R.id.botonMusica);
		if (musicaON) {

			buttonmusica.setBackgroundResource(R.drawable.pause);
			buttonmusica.setContentDescription(act
					.getString(R.string.DescriptionSonidoPause));
		} else {

			buttonmusica.setBackgroundResource(R.drawable.play);
			buttonmusica.setContentDescription(act
					.getString(R.string.DescriptionSonidoPlay));
		}

	}

	public void onStop() {
		if (siguientefase) {
			if (musicaON)
				act.stopService(music);
			contador.cancel();
		} else {
			pararJuego();
		}
	}

	public void onResume() {

		if (musicaON)
			musica(R.raw.backgroundsound);
		contador.start();
		if (js.getJuego().getPista() == 1) {

			mSensorManager.registerListener(mSensorListener,
					mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
					SensorManager.SENSOR_DELAY_UI);
		}

	}

	public void onPause() {

		if (musicaON)
			musica(R.raw.backgroundsound);
		contador.start();
		if (js.getJuego().getPista() == 1) {

			mSensorManager.unregisterListener(mSensorListener);
		}

	}

	public void atras() {
		pararJuego();
		Intent intent = new Intent(act, MainActivity.class);

		act.startActivity(intent);
		act.finish();
	}
}
